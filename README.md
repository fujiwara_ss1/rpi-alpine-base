### Alpine image for raspberrypi ###
raspberrypiの為のalpine imageです
[alpine公式](https://alpinelinux.org/downloads/)にあるMINI ROOT FILESYSTEM
のarmfhをダウンロードして作成した物です。

実行には
docker run -it --name containername takashifss/rpi-alpine-base sh
として立ち上げてください。

